# Convergence detection script...

This is work in progress. No guarantees!

## Things to try: 

To generate null distribution, compare:

 * Bootstrap by site without replacement (Current model)
 * Bootstrap with replacement
 * Randomize categorical assignment with existing data
 * Incorporate Blosum into null model or scoring?
 * Generate confidence intervals from the null distribution

Comparison with other methods

 * Elastic Net?
 * AIC via SURFACE [Link](http://onlinelibrary.wiley.com/doi/10.1111/2041-210X.12034/pdf)
 
 Comparative methods from PCM book.
 Ives AR, Garland T (2010) Phylogenetic logistic regression for binary dependent variables. Syst Biol 59(1):9–26. doi:10.1093/sysbio/syp074
 
 TRY phylolm in R
 * Ho LST, Ane C (2014) A linear-time algorithm for gaussian and non-gaussian trait evolution models. Systematic Biology
 
 http://cran.r-project.org/web/views/Phylogenetics.html
 
 Losos JB (1994) An approach to the analysis of comparative data when a phylogeny is
unavailable or incomplete. Syst Biol 43:117–123
Losos JB (2011) Convergence, adaptation, and constraint. Evolution 65:1827–1840
MuSSE simulate speciation and extinction.

Stats refs:

 * Bootstrap methods: [Link](http://www.stat.washington.edu/fritz/Reports/bootstrap-report.pdf)
  
![Metrics](https://bitbucket.org/beroe/converge/raw/master/images/converge_metrics_example.png "Metrics")
![Evaluation](https://bitbucket.org/beroe/converge/raw/master/images/ConvergeDetectOverview.jpg "Overview")

### Elastic approach
See elastic.R by Trevor Hastie, etc [Original Article](http://web.stanford.edu/~hastie/TALKS/enet_talk.pdf)

![Conceptual](https://bitbucket.org/beroe/converge/raw/master/images/elasticPlot2.png "Elastic plot")

