z = structure(c(2, 2, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 2, 4, 3, 5, 
6, 1, 2, 1, 2, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 3, 3, 2, 3), .Dim = c(6L, 
6L), .Dimnames = list(NULL, c("a", "b", "c", "d", "f", "g")))

depth = c(1, 1, 0, 0, 0, 0)

library(elasticnet)

# also glmnet

# elastic net is between LASSO and RIDGE, but handles correlated variables better

e=enet(x=z, y=depth,lambda=1)
e

# Call:
# enet(x = z, y = depth, lambda = 1)
# Sequence of  moves:
     # a f g c b d  
# Var  1 5 6 3 2 4 6
# Step 1 1 2 3 4 5 6

plot(e)

par(mfrow=c(1,2))
plot(e5,main="Lambda 0.5")
plot(e,main="Lambda 1")

# cross validation lets you select lambda (look for minimum)
library(glmnet)
cv.glmmod <- cv.glmnet(x,y=asthma,alpha=1)
plot(cv.glmmod)
best_lambda <- cv.glmmod$lambda.min

