library(ape)
library(geiger)
library(nlme)
library(phytools)

remove(guidetree)
remove(traittable)
remove(sites)

setwd("/Users/jwinnikoff/Documents/MBARI/converge/")

#ctenviro <- read.table("cteno_traits_2.tab", sep="\t", header=TRUE, row.names=1)
sites <- read.table("datasets/Converge_5col_example-JRW.tab", sep="\t", header=TRUE, row.names=1, stringsAsFactors = TRUE) 
sites <- Filter(function(x)(length(unique(x))>1), sites) # Remove 1-state columns
#column names prepended w/"X"
#columns of all "T" or all "F" get parsed as TRUE or FALSE (lol), but mathematically this shouldn't be a problem
#row.names(sites) <- sites[1]

colnames(fakeTraits) <- sub("X","Y",colnames(fakeTraits))
sites$sp = row.names(sites)

guidetree <- read.tree("datasets/12.tre")
#guidetree$tip.label[9] <- "Deep0" # A name that's in Steve's dataset but not Joe's

# Drop tips from test guide tree that are not in the test alignment
for(missing in setdiff(guidetree$tip.label, sites$sp)){
  guidetree <- drop.tip(guidetree, missing)
}

traittable <- merge(fakeTraits,sites,by="sp")
row.names(traittable) <- traittable$sp

name.check(guidetree, traittable) # Make sure trait table and tree tips agree

## HYPOTHESIS TESTING ##

glsList <- list("gls", length(colnames(sites)))
pValMin <- vector("numeric", length(colnames(sites)))
pValBonferroni <- vector("numeric", length(colnames(sites)))
gap <- vector("boolean", length(colnames(sites)))

for(i in 1:length(colnames(sites))){
  model <- paste("Y1 ~ ",colnames(sites)[i],sep="")
  #glsList[[i]] <- gls(as.formula(model), correlation = corBrownian(phy = guidetree), data = traittable, method = "ML", control = list(singular.ok = TRUE))
  glsModel <- gls(as.formula(model), correlation = corBrownian(phy = guidetree), data = traittable, method = "ML", control = list(singular.ok = TRUE))
  #capture.output(summary(glsModel), file="/Users/jwinnikoff/Documents/MBARI/PGLS/unittest/PGLSmodels.txt", append=TRUE)
  pValMin[i] <- min(as.data.frame.matrix(summary(glsModel)$tTable)[4])
  pValBonferroni[i] <- pValMin[i]*nrow(as.data.frame.matrix(summary(glsModel)$tTable)[4])
  #glsList[[i]] <- glsModel
}

alpha <- 0.01 # Set significance cutoff

## PLOTTING ##

neglog10pValBonferroni <- -log10(pValBonferroni)
position <- as.numeric(substr(colnames(sites), 2, nchar(colnames(sites))))

plotdata <- data.frame(position)
plotdata$neglog10pValBonferroni <- neglog10pValBonferroni
plotdata$pointColor <- ifelse(plotdata$neglog10pValBonferroni > -log10(alpha), "red", "black")

outData <- as.data.frame(position)
outData$pValBonferroni <- pValBonferroni

pplot <- qplot(position,neglog10pValBonferroni, color = pointColor, data=plotdata) + geom_line(color="red", alpha=(0.5), size=2)
pplot + scale_color_identity() + scale_x_continuous(limits=c(1,max(position, na.rm = TRUE)), breaks=seq(1,max(position, na.rm = TRUE), by=1)) + geom_hline(aes(yintercept=-log10(alpha), color="purple"))

