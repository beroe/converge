#!/usr/bin/env python

Usage = """
Plotlists -- used by converge_site to plot sequences and scores....
"""
	
def plotlists(Names,LOL,FileName="",Cols=[],Traits=[],Threshold=None,yesPDF=False):
	Colors=["#9978B0","#7CBB69",
	"#4B4C4C","#9B3520",
	"#BDA543","#4AA6C7","#5C79BC"]
	
	if yesPDF:
		from matplotlib import use
		use('PDF')   

	import matplotlib.pyplot as plt
	
	from matplotlib.lines import Line2D
	import numpy as np
	from pylab import subplot
	
	ColumnHead = dict(zip(range(len(Names)),Names))
	# ColumnHead ={1:'temp', 2:'sal', 3:'oxy'}
	
	# limits for the axes...
	# make this auto...?
	# AxisVals={1:(1,22), 2:(33.0,35.0), 3:(0,6)}
	MaxScore = max(sum(LOL,[]))
	Yax = (-.5,MaxScore+.5)
	# print 2*len(LOL[0])+1
	# print len(Names)+1
	if Cols:
		Width = len(Cols)+1
		# print len(Cols)
		# print type(Cols)
		if type(Cols[0]) is tuple:
			ColSet = set(sum(Cols,()))
		elif type(Cols[0]) is list:
			ColSet = set(sum(Cols,[]))
		else:
			ColSet = set(''.join(Cols))
		ColorDict = dict(zip(ColSet,Colors*7))
	else:
		Width = 8
	# Original way
	# fig,(ax2,ax)= plt.subplots(2,1,figsize=(Width,2*len(LOL[0])+1))
	
	# ax = fig.add_axes([0.1, 0.1, 8, 10]) # left, bottom, width height

	# Using Gridspec	
	from matplotlib import gridspec
	
	#TODO: Make this dynamic with figure height? 
	if len(Cols) > 30:
		fig = plt.figure(figsize=(10,18))
		gs = gridspec.GridSpec(2, 1, height_ratios=[2, 2]) 
	else:
		fig = plt.figure(figsize=(6,10))
		gs = gridspec.GridSpec(2, 1, height_ratios=[2, 2]) 
	# fig= plt.Figure(figsize=(8,8))
	ax = plt.subplot(gs[1])
	myalpha = 0.5
	
	
	
	for Num in range(len(Names)):
		mycolor = Colors[Num % len(Colors)]
		LineStyle = ['','-']['orm' in Names[Num]]+'-o' # --o or -o depending on label
		ax.plot(range(1,len(LOL[0])+1),LOL[Num],LineStyle ,label = Names[Num],color=mycolor,alpha=myalpha,linewidth= 4)
	
	if Threshold:
		ax.plot([.5,len(LOL[0])+.5],[Threshold,Threshold],':',color="red",alpha=.5,linewidth=2)
		for Xval,Point in enumerate(LOL[0]):
			if Point > Threshold:
				ax.plot(1+Xval,MaxScore+.4,'v',color="red",alpha=0.7,markersize=12,markeredgecolor='none')
		
	plt.xlim(.5,len(LOL[0])+.5)
	plt.ylim(Yax)
	fig.set_tight_layout(True)
	# plt.tight_layout()
	ax.xaxis.grid(True)
	# plt.grid(b=True, which='major', color='gray', linestyle='-')
	box = ax.get_position()
	ax.set_position([box.x0, box.y0+box.height*.2, box.width, box.height *.8])
	legend = ax.legend(ncol=4,loc='lower center',frameon=True,bbox_to_anchor=(0.5,-0.1))
	# legend = ax.legend(frameon=True, scatterpoints=1)
	light_grey = "#EEEEEE"
	rect = legend.get_frame()
	rect.set_facecolor(light_grey)
	rect.set_linewidth(0.0)

	if Cols:
		ax2 = plt.subplot(gs[0], sharex=ax)
		frame1 = fig.gca()
		frame1.axes.get_xaxis().set_visible(False)
		frame1.axes.get_yaxis().set_visible(False)
		frame1.axis('off')
		if Traits:
			TraitColors = ["#11111A","#FFFFFA","#882211"]
			# actually don't need the "len" part because zip will stop with the shortest
			TraitColor=dict(zip(sorted(set(list(Traits))),TraitColors[:len(set(Traits))]))
			# print Traits
			# print TraitColor
		if len(Cols)>30:
			SSize = 6
			FSize = 4
		else:
			SSize = 48
			FSize = 48
		##### FOR LOOP VERSION
		for C,S in enumerate(Cols):
			# print "C",C
			for R,L in enumerate(S):
				# print R, S
				if Traits:
					TColor = TraitColor.get(Traits[R],"red")
				else:
					TColor="white"
				# TODO: Make the marker and box size change with number of seqs
				ax2.plot(C+1,(len(S)-R)-1,'s',markerfacecolor=ColorDict[L],
				markeredgecolor='none',alpha=0.5,markersize=SSize)
				ax2.text(C+1,(len(S)-R)-1,L,color = TColor,
				        family="CourierPCfB",ha='center',va='center',
				        fontsize=FSize, fontweight='bold',alpha=0.8)
		#### VECTOR VERSION
		""" NOT WORKING -- Make into rows and just loop through them instead of nested"""
		# w = len(Cols)
		# h = len(Cols[0])
		# Rows = zip(*Cols)
		# 
		# if Traits:
		# 	TColor = [TraitColor.get(Tr,"blue"), for Tr in Traits]
		# else:
		# 	TColor = ["white"] * h
		# xs = range(1,w+1)
		# ys = range(1,w)
		# ax2.plot(xs,ys,'s',markerfacecolor=Colors[0],markeredgecolor='none',alpha=0.5,markersize=SSize)
		# ax2.text(xs,ys,,color = TColor,
		# 		        family="CourierPCfB",ha='center',va='center',
		# 		        fontsize=FSize, fontweight='bold',alpha=0.8)
		
		plt.xlim(0.5,len(Cols)+.5)
		Yax = (-.5,len(Cols[0]))
		plt.ylim(Yax)


	if yesPDF:
		plt.savefig('Plot_Con'+FileName+str(Names[0])+".pdf")
		plt.close()
	else:
		plt.show()
	

def main():

	Names = ["MyChiNorm","ChiList","ChiNorm","YatesList","YatesNorm","Gstat","Fisher","InvFish"]
	LOL=[[3.00, 2.33, 1.20, 2.00, 1.00, 1.00, 1.00, 1.67],
	[6.00, 3.00, 6.00, 1.50, 6.00, 6.00, 3.75, 0.00],
	[3.00, 1.50, 1.20, 0.75, 1.00, 1.00, 0.75, 0.00],
	[2.34, 0.75, 2.06, 0.09, 0.75, 0.75, 0.56, 0.75],
	[1.17, 0.38, 0.41, 0.05, 0.13, 0.13, 0.11, 0.38],
	[3.82, 1.91, 3.82, 1.05, 3.82, 3.82, 2.43, 0.00],
	[0.07, 0.20, 0.07, 0.40, 0.07, 0.07, 0.13, 0.60],
	[0.93, 0.80, 0.93, 0.60, 0.93, 0.93, 0.87, 0.40]]
	Cols=('AABBBB', 'EEBCCC', 'EEEBBB', 'EBCCCC', 'EEACDC', 'EEACDB', 'EEACED', 'EBACDF', 'BABABA')
	plotlists(Names,LOL,Cols = Cols,Traits = list('DDSMSSSS'),Threshold=0.5,yesPDF=False)

if __name__ == '__main__':
	main()