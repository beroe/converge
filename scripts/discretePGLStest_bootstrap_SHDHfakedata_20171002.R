library(ape)
library(geiger)
library(nlme)
library(phytools)
library(reshape2)

remove(guidetree)
remove(traittable)
remove(sites)

setwd("/Users/jwinnikoff/Documents/MBARI/converge/")

# Load simulated depth data
nsims <- 200
fakeTraits <- read.table("datasets/simDepths.tab", header=TRUE)
fakeTraits <- fakeTraits[(ncol(fakeTraits)-nsims):ncol(fakeTraits)]

sites <- read.table("datasets/Converge7state-JRW.tab", sep="\t", header=TRUE, row.names=1, stringsAsFactors = TRUE) 
sites <- Filter(function(x)(length(unique(x))>1), sites) # Remove 1-state columns
#column names prepended w/"X"

colnames(fakeTraits) <- sub("X","Y",colnames(fakeTraits))
sites$sp = row.names(sites)

guidetree <- read.tree("datasets/12.tre")
#guidetree$tip.label[9] <- "Deep0" # A name that's in Steve's dataset but not Joe's

# Drop tips from test guide tree that are not in the test alignment
for(missing in setdiff(guidetree$tip.label, sites$sp)){
  guidetree <- drop.tip(guidetree, missing)
}

traittable <- merge(fakeTraits,sites,by="sp")
row.names(traittable) <- traittable$sp

name.check(guidetree, traittable) # Make sure trait table and tree tips agree

## HYPOTHESIS TESTING ##
alpha <- 0.05 # Set significance cutoff

sites <- subset(sites, select = -c(sp)) # Drop sp column *after* merge
position <- as.numeric(substr(colnames(sites), 2, nchar(colnames(sites)))) # This is a numeric vector that excludes 1-state positions

plotdata <- list() #init
plotdata["position"] <- list(position) # a list of lists to be filled with bootstrapped -log10(P) values

for(fakeDepths in colnames(traittable[2:ncol(fakeTraits)])){
  #pValMin <- vector("numeric", length(colnames(sites)))
  pValBonferroni <- vector("numeric", ncol(sites))
  glsList <- list("gls", ncol(sites))
  for(i in 1:ncol(sites)){
    model <- paste(fakeDepths,colnames(sites)[i],sep=" ~ ")
    glsModel <- gls(as.formula(model), correlation = corBrownian(phy = guidetree), data = traittable, method = "ML", control = list(singular.ok = TRUE))
    #capture.output(summary(glsModel), file="/Users/jwinnikoff/Documents/MBARI/PGLS/unittest/PGLSmodels.txt", append=TRUE)
    pValMin <- min(as.data.frame.matrix(summary(glsModel)$tTable)[4])
    pValBonferroni[i] <- pValMin*nrow(as.data.frame.matrix(summary(glsModel)$tTable)[4]) # Multiply by # of t-tests
    glsList[[i]] <- glsModel # To store complete gls model objects for breakout/debugging
  }
  neglog10pValBonferroni <- -log10(pValBonferroni)
  plotdata[fakeDepths] <- list(neglog10pValBonferroni)
}

## PLOTTING ##

plotdata <- as.data.frame(plotdata)
plotdata$position <- as.factor(plotdata$position)
plotdata.molten <- melt(plotdata, id.vars = "position")
ggplot(plotdata.molten, aes(position, value)) + geom_violin(color="white",fill="red",alpha=0.3) + scale_color_identity() + geom_hline(aes(yintercept=-log10(0.05), color="firebrick3")) + geom_hline(aes(yintercept=-log10(0.01), color="cadetblue")) + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),panel.background = element_blank(), axis.line = element_line(colour = "black"))

