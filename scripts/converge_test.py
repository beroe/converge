#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""converge_test .py  
 
 Generate three matrices: one with a large number of high-scoring columns, 
 one with medium, and one with large number of low-scoring columns.
 
 Generate those three matrices of different lengths for testing normalization
 
 Generate score of those matrices, using just Residual and MyChi
 
 !. Shuffle the positions in each column to generate 1000 variations of each matrix
 2. Score each shuffled matrix and save that to generate a distribution of scores
 3. Compare the score of the HML matrices to those scores.
 
 
 """

import sys
from math import log
import converge_site as cs
import random
import argparse

DEBUG = False

def get_options():
	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument("-r", "--resample", type=int, dest="HowMany", default = 10, help="How many randomly shuffled samples to make")
	parser.add_argument("-m", "--multipy", type=int, dest="multiplier", default = 2, help="How many times to multiply the columns to make larger genes")	
	parser.add_argument("-b", "--debug", action="store_true", dest="DEBUG",default=False, help="Turn on Debugging output")
	# parser.add_argument("FileList", nargs='+')
	options = parser.parse_args()
	return options


def MakeSimData(mult=2):
	"""use multiplier times 10 as the total number of columns"""
	# Columns 0-4 in order of best to worst scoring
	TypeColumns = ["AAEEEE","BBECCC","BBBCCC","ABCDEF","ACACAC"]
	High   = [0,0,0,0,1,1,2,2,3,4]
	Medium = [0,0,1,1,2,2,3,3,4,4]
	Low    = [0,1,2,2,3,3,4,4,4,4]
	High   = [0,0,1]
	Medium = [1,2,3]
	Low    = [3,4,4]
	HighMat   = [TypeColumns[x] for x in High]   * mult
	MediumMat = [TypeColumns[x] for x in Medium] * mult
	LowMat    = [TypeColumns[x] for x in Low]    * mult
	
	if DEBUG:
		print >> sys.stderr, HighMat
		print >> sys.stderr, MediumMat
		print >> sys.stderr, LowMat
	return HighMat, MediumMat, LowMat
#MC, MG = cs.MyChiSq2(Ol,El)  # MyChi and GStat
#RL     = cs.getResiduals(Ol,El)

def ShuffleColsWithoutReplace(Matrix = []):
	LenCol = len(Matrix[0])
	NewMat = [random.sample(OneCol,LenCol) for OneCol in Matrix]
	return NewMat

def ShuffleColsWithReplace(Matrix = []):
	LenCol = len(Matrix[0])
	NewMat = [ [random.choice(OneCol) for _ in xrange(LenCol)] for OneCol in Matrix]
	return NewMat
	
	
def getScores(TraitSets,Traits,Cols):
	RList = []
	MyChiList = []
	
	ActualList = cs.getFullActual(TraitSets,Traits,Cols,KeepDash=False)
	ColSets = [ tuple(set(x)) for x in Cols]
	Ex,Eli = cs.getExpect(ColSets,Cols)
	ECount = cs.getChiExpect(ActualList)
	
	## CHECK THE -1 SCORES
	
	# print "Actual",ActualList
	# print "ECount",ECount
	# print "Eli",Eli
	
	for Ol,El,mEl in zip(ActualList,ECount,Eli):	
		RList.append(cs.getResiduals(Ol,El))
		MyChiList.append(cs.MyChiSq(Ol,mEl))
	totalR = sum(RList)
	totalMy = sum(MyChiList)
	
	# TODO: probably also want to normalize or save length
	
	# print "RList",RList
	# print "MyChi",MyChiList
	# print "RM:",totalR, totalMy
	return totalR, totalMy
	
	
	
def main():
	Options = get_options()
	DEBUG = Options.DEBUG
	HowMany = Options.HowMany
	multiplier = Options.multiplier
		
	Traits = list("DDSSSS")
	TraitSets = tuple(set(Traits))
	Names = [x + str(d) for d,x in enumerate(Traits)]

	ReRead = False
	# HM, MM, LM = MakeSimData()
	Header = "OrigSim\tTreat\tResid\tMyChi\tResidRepl\tMyChiRepl"
	print Header
	print "# ==============================="
	for Mat,HML in zip(MakeSimData(mult=multiplier),["High","Med","Low"]):
	
		if ReRead:
			TCol = sorted(zip(Traits,Names,Mat))
			# print >> sys.stderr, "TC",TCol
	
			Traits,Names,Seqs = zip(*TCol)
			# JUST FOR SIMULATED DATA: 
			Cols = zip(*Seqs)
		else:
			Cols = Mat
		
		# print >> sys.stderr, "Cols",Cols

		R,M = getScores(TraitSets,Traits,Cols)
	
		if DEBUG:
			print >> sys.stderr, Mat

		print "Orig\t{}\t{}\t{}\t{}\t{}".format(HML,R,M,R,M)		
		
		for V in range(HowMany):
			# In here the simulated results will be calculated
			SCols  = ShuffleColsWithoutReplace(Mat)
			SRCols = ShuffleColsWithReplace(Mat)
			if DEBUG:
				print >> sys.stderr, "ShuffleColRepl", ["".join(C) for C in SRCols]
			R,M   = getScores(TraitSets,Traits,SCols)
			RR,MR = getScores(TraitSets,Traits,SRCols)
			print "Sim\t{}\t{}\t{}\t{}\t{}".format(HML,R,M,RR,MR)
		print "# -------------------------------"
		 

main()
		
	
""" Output from converge_site test data parsing;
Names ['D1', 'D5', 'S2', 'S3', 'S4', 'S6'] 
Sequences ['ABBAA', 'ABBBE', 'EEBCA', 'ECCDE', 'ECCEA', 'ECCFE'] 
Traits ['D', 'D', 'S', 'S', 'S', 'S'] 
TCol [('D', 'D1', 'ABBAA'), ('D', 'D5', 'ABBBE'), ('S', 'S2', 'EEBCA'), ('S', 'S3', 'ECCDE'), ('S', 'S4', 'ECCEA'), ('S', 'S6', 'ECCFE')] 
TraitSets ('S', 'D') 
ActualList [[[0, 4], [2, 0]], [[3, 0, 1], [0, 2, 0]], [[3, 1], [0, 2]], [[0, 1, 0, 1, 1, 1], [1, 0, 1, 0, 0, 0]], [[2, 2], [1, 1]]] 

"""