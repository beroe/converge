#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Some options
   -h Show all options
   -f Input filename of alignment (should be full length padded)
   -d File with Name<tab>Category
   
Site corr:
Starting with an alignment, and a categorical data file
For each position, check if it is invariant or informative.

Calculate the Metric for each column:
Based on correlation between the categories (CAT) and the AA.
Use a substitution model (WAG/JTT) to measure how likely the differences are

Use the distribution of categories to weight the expectation

Sum the STAT across all columns (?) Normalized by lenght (average?) 
to find genes that have more correlation than expected 
(Use the species tree as the null hypothesis?) But that will be influenced
by the factor as well.

Or just add up 1/0 when they are the same?

Find the most common trait for each CAT.
Add up number of "matches"/consistent scores.
Then add up number of "matches" of the other CAT *which don't match the first*
Check both ways (letting state B "choose" which is higher for it) 
and then add up the opposites for the other state.

Between the two, choose the one that gives the highest score...

(What about 3,4,or 5-state traits? As long as one correlates with a state vs another)

If there is more of one cat, then that will always end up determining its dominat char.
Is that good or bad?

1/2 is the lowest value: adjust somehow (n-1) so that goes to zero?
What about (E-O^2)/E again?
Future, weight by AA transisions!
 similarity -- more similar and lower barrier to switch (higher WAS
frequency?) - 

Expectation: 1 / number of traits?
Determined for each trait:
number of occurrences / number of taxa

That multiplied by the number of taxa in CAT = expected number

Potential test: http://udel.edu/~mcdonald/statkruskalwallis.html
Similar? http://stats.stackexchange.com/questions/63078/correcting-a-dna-scoring-algorithm-for-scanned-sequence-length-consensus-bindin

https://en.wikipedia.org/wiki/Mutual_information


Long branch index
LB = (PDi/PDave -1)*100
# CHECK: phylogenetic least squares?
Chi-square: should have expected frequencies of at least 5 for 80%% of classes
"""

# TODO: Generate plot file name from input file name
# NOTE Handle gaps in algorithm... # Sort of incorporated now...
# TODO: weight scores by JTT or WAG
# TODO: Make subplot size correct
# IDEA: CHoose one score! Make G-stat for MyChi?
# TODO: *** Run a test with RFP GFPs!
# TODO: use the full columns so you can output the position of the above threwhold scores -- sort of done
# TODO: Find max value for state with min occurrences -- another metric, to avoid tallying one-offs
#	Use that to "fix" the expectation. The other one must be not in that state
#	I.e., don't add up all 4 scores, but just the ones that differ
#	Important for the case of
#	 0	1	1	1	1
#	 2	0	0	0	0


import sys
import argparse
from math import log

# Oh well, using numpy, etc...
# easy_install -U scikit-learn

# also imports home-made module below: plotlists

def ParseData(DataMatrix=""):
	if not DataMatrix:
		DataMatrix = """>D1
ABBACAAABAA-
>D5
ABBACBBBEBA-
>S2
EDBBCCCBEB-B
>S3
EEECCDDCBBBE
>S4
EEEDEDEDBBBE
>S6
EEEEEFFFEBBE"""
		DataMatrix = """>D1
ABBAA
>D5
ABBBE
>S2
EEBCA
>S3
ECCDE
>S4
ECCEA
>S6
ECCFE"""
	Names= []
	Seqs = []
	Traits = []

	for L in DataMatrix.split('\n'):
		L=L.rstrip()
		if L.startswith(">"):
			Names.append(L[1:])
			Traits.append(L[1])
		else:
			Seqs.append(L)
	return Names,Seqs,Traits

def logout(s,t='',u='',OverRide=False):
	if DEBUG or OverRide:
		print >> sys.stderr, s,t,u
	
def getExpect(ColSet,Cols):
	L = float(len(Cols[0]))
	E = []
	ECList = []

	for S, Col in zip(ColSet,Cols):
		EC = [ Col.count(C) for C in S ]
		ECList.append(EC)
		Expect = [ ECt/L for ECt in EC ]
		E.append(Expect)
		# EC = [ Col.count(C)*CC/L for C,CC in zip(S,SC) ]		
	return E,ECList


def get_options():
	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument("-f", "--seqfile", dest="SeqFileName", help="Path to alignment file")
	parser.add_argument("-d", "--datafile", dest="DataFileName", help="File with Name:Character; otherwise use first char")	
	parser.add_argument("-t", "--threshold", dest="ThresholdValue", help="Threshold value")	
	parser.add_argument("-p", "--makeplot", action="store_true", dest="PLOTTHEM", default=True, help="Print header line before output")
	parser.add_argument("-s", "--saveplot", action="store_true", dest="yesPDF",default=False, help="Save Plot to PDF")
	parser.add_argument("-b", "--debug", action="store_true", dest="DEBUG",default=False, help="Turn on Debugging output")
	parser.add_argument("-l", "--debuglight", action="store_true", dest="DEBUGLIGHT",default=False, help="Turn on light Debugging output")
	parser.add_argument("-o", "--plotone", action="store_true", dest="PLOTONE",default=False, help="Plot only one value")
	parser.add_argument("FileList", nargs='+')
	# options = parser.parse_args()
	options = parser.parse_args()
	return options




# def get_options():
# 	parser = OptionParser(usage = __doc__)
# #	parser.add_option("-v", "--version", action="store_true", dest="version", default=False, help="Print program version")
# 	parser.add_option("-f", "--seqfile", action="store", type="string", dest="SeqFileName", help="Path to alignment file")
# 	parser.add_option("-d", "--datafile", action="store", type="string", dest="DataFileName", help="File with Name:Character; otherwise use first char")
# 	parser.add_option("-t", "--threshold", action="store", type="string", dest="ThresholdValue", help="Threshold value")
# 	parser.add_option("-p", "--makeplot", action="store_true", dest="PLOTTHEM", default=True, help="Print header line before output")
# 	parser.add_option("-s", "--saveplot", action="store_true", dest="yesPDF",default=False, help="Save Plot to PDF")
# 	parser.add_option("-b", "--debug", action="store_true", dest="DEBUG",default=False, help="Turn on Debugging output")
# 	parser.add_option("-l", "--debuglight", action="store_true", dest="DEBUGLIGHT",default=False, help="Turn on light Debugging output")
# 	parser.add_option("-o", "--plotone", action="store_true", dest="PLOTONE",default=False, help="Plot only one value")
# 	(options, args) = parser.parse_args()
# 	return options, args
	
	
# find the Observed number of each ColSet for each Trait
# For now, show E, O, and (E-O)^2/E for each Column and Trait
# probably better for memory to the all the calcs in this 
# function and just save the result, rather than saving the 
# whole list of scores.

def getElasticnet():
	"""do in a separate program"""
	pass
	
	
def getFullActual(TraitSets,Traits,Cols,KeepDash=False):
	ScoreList = []
	CL = len(Cols[0])
	BlankApp = [[-1,-1]] * len(TraitSets)
	for C in Cols:
		# Not more than half dashes
		GotScore = False
		if KeepDash or C.count('-') < CL * 0.5:
			S = set(C)
			S.discard('-')
			if len(S) > 1:  # not just one state
				F = [C.count(B) for B in S] 
				if not(len(F) == 2 and 1 in F): # All the same but one SNP
					Scores = {}
					for Item,Value in zip(Traits,C):
						Scores[(Item,Value)] = Scores.get((Item,Value),0) + 1

					ActL=[] 
					for T in TraitSets:
						ActL.append( [ Scores.get((T,AA),0) for AA in S] )
					ScoreList.append(ActL)
					GotScore = True
		if not GotScore:
			ScoreList.append(BlankApp)
	return ScoreList

	############ REMOVE SINGLE DIFFS ###############
	#
	Freqs = [[X.count(B) for B in set(X) ] for X in NewCols] 
	SNPcount = len(Cols)-len(NewCols)-Invariant


    ######### REMOVE MORE THAN HALF DASHES ################
	LC = len(NewCols[0])*.5
	NewCols = [ C for C in NewCols if C.count('-') < LC]
	DashyCount = len(Cols)-len(NewCols)-Invariant-SNPcount
		  

	return ScoreList
	
def getActual(ColSets,Cols,TraitSets,Traits):
	# get Actual counts for each column. Don't calc yet.
	# generates a contingency table
	ScoreList = []
	for CSet,C in zip(ColSets,Cols):
		Scores = {}
		for Item,Value in zip(Traits,C):
			Scores[(Item,Value)] = Scores.get((Item,Value),0) + 1

		ActL=[] 
		for T in TraitSets:
			ActL.append( [ Scores.get((T,AA),0) for AA in CSet if AA != '-'] )
		ScoreList.append(ActL)

	return ScoreList

def getChiExpect(ActList):
	"""Generates the table for testing Independence"""
	EList = []
	for Act in ActList:
		rowsums = [sum(a) for a in Act]
		colsums = [sum(b) for b in zip(*Act)]
		total = float(sum(rowsums))
		el = [[x*y/total for x in colsums] for y in rowsums]
		EList.append(el)
	return EList

def getMyExpect(ActList):
	
	pass

def product(mylist):
    p = 1
    for i in mylist:
        p *= i
    return p

def fishers(ActList):
	""" Fishers: (r1!)(r2!)(c1!)(c2!) / n! a! b! c! d!"""
	from math import factorial as f
	FList = []
	for Act in ActList:
		if max(Act[0]) > 0:
			rowsums = [sum(a) for a in Act]
			colsums = [sum(b) for b in zip(*Act)]
			total = float(sum(rowsums))
			topfacts = [f(rs) for rs in rowsums + colsums]
			topline = product(topfacts)
			# fancy way to flatten list
			flatlist = sum(Act,[])
			bottomfacts = [f(bs) for bs in flatlist + [total]]
			bottomline = product(bottomfacts)
			fstat = topline / float(bottomline)
			FList.append(fstat)
		else:
			FList.append(0)
	return FList
	

def printlists(mylist,horizon=True):
	""" print tabulated numerical lists for lists or list of lists (one deep)"""
	delim = ['\n','\t'][horizon]
	if type(mylist[0]) is list:
		for x in mylist:
			outstr = delim.join([["%.2f",'%d'][type(v) is int]%v for v in x])
			print outstr
	else:
		outstr = delim.join([["%.2f",'%d'][type(v) is int]%v for v in mylist])
		print outstr
		
def ChiSq(Olist,Elist):
	""" Calculate ChiSq, Gstat (MI), Etc from a table of expected and observed"""
	# logout("Olist",Olist)
	# logout("ChiElist",Elist)
	L = len(Elist[0])
	ChiSum = 0
	YatesSum = 0
	YatesNorm = 0
	ChiNorm = 0
	GStat = 0
	GStat2 = 0
	Yates = False
	# flatten list
	if Olist[0]:
		try:
			if min(sum(Olist,[])) > 5:
				Yates = True
		except ValueError:
			print >> sys.stderr, "NO OLIST",Olist
		for Oelem,Eelem in zip(Olist,Elist):
			for O,E in zip(Oelem,Eelem):
				E = float(E) 
			
				# Times two at the end
				if O>0:
					GStat += O * log(O/E)  # this is the natural log
					GStat2 += O * log(O/E,2) # this is base2
				#  MI = G/2n ??
				if E > 0:
					ChiSum += ((O-E)**2)/E
					ChiNorm += ((O-E)**2)/(E*L)
					YatesSum += ((abs(O-E)-.5)**2)/(E)
					YatesNorm += 2*((abs(O-E)-.5)**2)/(E*L)
		if Yates:
			print >> sys.stderr, "Recommended to use Yates",Olist
	# GStat would normally be 2x and MI is GStat/2n, but skipping
	# GStat *= 2
	return ChiSum,ChiNorm,YatesSum,YatesNorm,GStat,GStat2

def getResiduals(Olist,Elist):
	""" 
	Calculates the Adjusted Residuals for each cell and returns the 
	absolute value of the difference between max and min...
	
	fo-fe, normalized by sqrt(fe (1-Orowproportion)(1-Ocolproportion))
	
Example from http://www.brynmawr.edu/socialwork/GSSW/Vartanian/Handouts/Residual.pdf

TO = [ [50,40],[50,60],[30,70] ]
TE = [ [39,51],[47.67,62.33],[43.33,56.67] ]

Example from Scheaffer "Categorical Data Analysis"
TO=[[279,73,225],[165,47,191]]
TE = [[261.41632653061225, 70.65306122448979, 244.93061224489796],
  [182.58367346938775, 49.3469387755102, 171.06938775510204]]

	"""
	# implemented only for 2 states!
	RSums = [ sum(r) for r in Olist ]
	CSums = [ sum(c) for c in zip(*Olist) ]
	Total = float(sum(RSums))
	# print RSums,CSums,Total

	# TotalList = []
	# ** Actually for 2 categories only need to do this once 
	# because the second row is negative of the first...
	
	# Oelem,Eelem,R = Olist[0],Elist[0],RSums[0]
	if min(Olist[0])>=0 and len(Olist) == 2:
		for Oelem,Eelem,R in zip(Olist,Elist,RSums):
			RList = []
			# print "Oelem",Oelem
			for O,E,C in zip(Oelem,Eelem,CSums):
				Calc = (O-E) / (E * (1 - R/Total) * (1-C/Total))**0.5
				RList.append(Calc)
			Diff = abs(min(RList)-max(RList))
		# actually probably want to do some investigations here and return some other metric
		
		return Diff
	else:
		return -1 
	
	
def getMyExpect(Olist):
	""" all Eli (colsums) must be > 1
	only one-tailed expectations: greater than expected """
	pass
	
	
def MyChiSq(Olist,NewElist):
	## NOT USED
	""" TODO find the less-frequent state and its most popular trait. 
	find the max of the other states which are not that state?? What if most of deep have
	one trait, but a few have a different one, but it is unique?
	"""
	# logout("Olist   ",Olist)
	# logout("NewElist",NewElist)
	
	MyChiSum = 0
	L = float(len(NewElist))
	for Oelem in Olist:
		for O,E in zip(Oelem,NewElist):
			MyChiSum += 2*((O-E)**2)/(E*(L))
	return MyChiSum

def MyChiSq2(Olist,NewElist):
	"Using different expect value and dividing by number of states"
	""" TODO find the less-frequent state and its most popular trait. 
	find the max of the other states which are not that state?? What if most of deep have
	one trait, but a few have a different one, but it is unique?
	"""
	# logout("Olist   ",Olist)
	# logout("NewElist",NewElist)
	MyChiSum2 = 0	
	MyGSum = 0
	L = float(len(NewElist))
	# Try one-tailed text 
	for Oelem,Eelem in zip(Olist,NewElist):
		for O,E in zip(Oelem,Eelem):
			if (O>1) and (O>E):
				MyGSum += O * log(O/E)  # this is the natural log
				MyChiSum2 += ((O-E)**2)/(E*(L)) # 2 is just for scaling on graph
	return MyChiSum2,MyGSum


def MutualInfo(ActList):
	""" use log2 """
	""" Reality check, R gives these MI values
	
	Probably not great -- see the G stat calculated by the ChiSq function
	
	require(entropy)
	mi.empirical(M)
	[1] 0.3182571
	> M
	  A B
	A 3 1
	B 0 2
	> M=as.table(rbind(c(2,1,1),c(0,1,1)))
	> mi.empirical(M)
	[1] 0.174416
	> M
	  A B C
	A 2 1 1
	B 0 1 1
	Typically one normalized by the sum of the entropies or by the joint entropy. Sum of entropies is a bit better because H(Xi,Xj)≤H(Xi)+H(Xj)
	shortcut formula: 
	log(n) + (A - B - C)/n
	A = sumOverxR,yC of (kxy)*log(kxy)
	B = sumOverxR of (kx*)*log(kx*) and C is same for Y and cols.
	R = states of all var1
	C = unique states of var2
	k(x,*) = frequency of each element of R
	k(y,*) = frequency of each element of C
	k(xy) = frequency of each combo of R,C
	
	In mine, R will have more dimensions that C
	Can it tolerate zeroes!?
	
	http://www.inf.ufrgs.br/pln/nlp/index.php?option=com_content&view=article&id=32&Itemid=29
	
	"""
	
def getConcord(ActualList):
	""" 
	This is supposed to be ordered data. Maybe sort by BLOSUM scores?
	For a table [a,b],[c,d] the traits have to sortable and C=a*d are concordant while D=b*c are discordant
	
	Gamma:  the difference C-D/C+D
	Kendall’s tau-b,
	
	Sort traits from lot to high, high to low
	
	Highest of top row *(sum of bottom row below that) + next highes *(values below that)
	Use less numerous trait (or deep) as main trait
	
	Deep     |  3 5 7 9   Have to stay coordinated....
	Shallow  | 10 8 6 3
	
	Calculate Gamma from that... ()
	"""
	CList = []
	GammaList = []
	for A in ActualList:
		if not A[0] or max(A[0])<0:
			CList.append(0)
			GammaList.append(0)
		else:
			# logout(".....")
			# logout("A item",A)
			ol = sorted(zip(*A), key=lambda x: (x[0], -x[1]))
			TopSort = zip(*ol)
			
			### REMOVE ME or figure a way to sort by AA power? 
			# TopSort = A
			# print( "TopSort",TopSort)

		
			ol = sorted(zip(*A), key=lambda x: (-x[1],x[0]))
			BotSort = zip(*ol)
			CSum = 0
			DSum = 0
			for ind in range(len(A[0])):
				# print TopSort[0][-(ind+1)]
				# print TopSort[1][:-(ind+1)]
				# print '...'
				CSum += TopSort[0][-(ind+1)] * sum(TopSort[1][:-(ind+1)])
				DSum += TopSort[0][ind] * sum(TopSort[1][(ind+1):])
			# print >> sys.stderr, (CSum, DSum)
			if CSum or DSum:
				Inverter = [1,-1][DSum>CSum]
				Gamma = (CSum-DSum)*Inverter/float(CSum+DSum)
			else:
				Gamma = 0.00
			GammaList.append(Gamma)
			try:
				Adjuster = 1.0
				ConRatio = CSum/float(len(A[0]))/Adjuster
				CList.append(ConRatio)
			except ZeroDivisionError:
				print >> sys.stderr, "ERROR",A
	return CList,GammaList


def getOddsRatio(ActualList):
	# ratio of odds for group 1 / group 2
	pass

def getNumberSame(Olist):
	""" figure the number that is the same in each category 
	compared with how many would be expected to be the same"""
	RSums = [ sum(r) for r in Olist ]
	CSums = [ sum(c) for c in zip(*Olist) ]
	Total = float(sum(RSums))
	if len(Olist) == 2:
		# Find the index of the smallest category
		FirstInd = RSums.index(min(RSums))
		SecondInd = 1 - First
		x,y = ActualList[First],ActualList[Second]
		msx = [(max(x),sum(x)) for x in ActualList]
		print msx
		sx = sum(x)
		ix = x.index(max(x))
		return sx,msx,ix
	else: 
		return -1,-1,-1
	
	
	
def CleanSeqs(Cols):
	####### REMOVE INVARIANT ###########
	# Code to remove cols all the same (SNPs might still be informative)
	# Might not want to do this, to preserve position numbers?
	NewCols = [ C for C in Cols if len(set(C)) > 1 + ('-' in C)]
	Invariant = len(Cols)-len(NewCols)

	############ REMOVE SINGLE DIFFS ###############
	#
	Freqs = [[X.count(B) for B in set(X) ] for X in NewCols] 
	NewCols = [C for F,C in zip(Freqs,NewCols) if not(len(F) == 2 and 1 in F)]
	SNPcount = len(Cols)-len(NewCols)-Invariant


    ######### REMOVE MORE THAN HALF DASHES ################
	LC = len(NewCols[0])*.5
	NewCols = [ C for C in NewCols if C.count('-') < LC]
	DashyCount = len(Cols)-len(NewCols)-Invariant-SNPcount	
	return NewCols,Invariant,SNPcount,DashyCount

	
def main():
	Colors=["#9978B0","#7C8869","#4B4C4C","#9B3520","#BDA543","#4B4C4C","#5C79BC"]
	global DEBUG
	if len(sys.argv)<2:
		sys.stderr.write(__doc__)
		PLOTTHEM = True
		PLOTONE = False
		DEBUG = False
		DEBUGLIGHT = True
		yesPDF = False
		FileName = ''
		Threshold = 3.5
		
	else:
		Options = get_options()
		PLOTTHEM = Options.PLOTTHEM
		PLOTONE = Options.PLOTONE
		DEBUG = Options.DEBUG
		DEBUGLIGHT = Options.DEBUGLIGHT
		yesPDF = Options.yesPDF
		FileName = Options.SeqFileName
		if Options.ThresholdValue:
			Threshold = float(Options.ThresholdValue)
		
	DEBUG = True
	if (FileName):
		
		with open(FileName,'rU') as sf:
			Names = []
			Seqs = []
			for L in sf:
				L = L.rstrip()
				if L.startswith(">"):
					Names.append(L[1:])
					Seqs.append("")
				elif L:
					Seqs[-1] += L					
					
		if (Options.DataFileName):
			with open(Options.DataFileName,'rU') as df:
				ND = {}
				for L in df:
					L = L.rstrip()
					if L:
						Name,Char = L.replace(",","\t").split("\t")
						ND[Name]=Char
				try:
					Traits = [ND[n] for n in Names]
				except KeyError:
					sys.exit("ERROR: No Trait found for {}\n".format(n))
				
		else: # No Data File given...
			Traits = [Na[0] for Na in Names]
			sys.stderr.write("Using first character for trait groups. Specify tab-delimited file with [-d].\n")

		
	else:
		Names,Seqs,Traits = ParseData()
		sys.stderr.write("You must specify a data file [-f].\nRunning with test data...\n")
		
	
	logout("Names",Names)
	logout("Sequences",Seqs)
	logout("Traits",Traits)
		
	# print Names
	# print Seqs
	# print " ".join(Traits)
	# print ". "* len(Traits)
	
	# Get traits together
	
	TCol = sorted(zip(Traits,Names,Seqs))
	logout("TCol",TCol)
	Traits,Names,Seqs = zip(*TCol)
	
	Cols = zip(*Seqs)
	
	TraitSets = tuple(set(Traits))
	logout("TraitSets", TraitSets)

	UseOld = False
	if UseOld:
		NewCols,Invariant,SNPcount,DashyCount = CleanSeqs(Cols)
		ActualList = getActual(ColSets,NewCols,TraitSets,Traits)
	else:
		ActualList = getFullActual(TraitSets,Traits,Cols,KeepDash=False)
		NewCols=Cols
		logout("ActualList",ActualList)

	if DEBUGLIGHT:
		for Tr,Co in zip(Traits,zip(*NewCols)):
			print "  %s | "%Tr,"".join(Co)
	
	ColSets = [ tuple(set(x)) for x in NewCols]
	# logout("ColSets", ColSets)


	TraitCount = [Traits.count(T) for T in TraitSets]
	logout("TraitCount", TraitCount)
	

	ConList,GammaList = getConcord(ActualList)
	
	Ex,Eli = getExpect(ColSets,NewCols)
	# logout("Ex",Ex)
	# logout("Eli",Eli)
	#
	ECount = getChiExpect(ActualList)
	print >> sys.stderr, ("ECount",ECount)

	# logout("ActualList:",ActualList)
	ChiList = []
	ChiListNorm = []
	YatesList = []
	YatesListNorm = []
	GList = []
	GList2 = []
	MyChi2List = []	
	MyChiList = []
	MyGList = []
	RList = []
	
	for Ol,El in zip(ActualList,ECount):
		CL,CLN,YL,YLN,GT,GT2 = ChiSq(Ol,El)
		ChiList.append(CL)
		ChiListNorm.append(CLN)
		YatesList.append(YL)
		YatesListNorm.append(YLN)
		GList.append(GT)
		GList2.append(GT2)
		
		MC,MG = MyChiSq2(Ol,El)
		MyChi2List.append(MC)
		MyGList.append(MG)
		
		RList.append(getResiduals(Ol,El))
		
	
	for mOl,mEl in zip(ActualList,Eli):
		MyChiList.append(MyChiSq(mOl,mEl))
	
	Flist = fishers(ActualList)
	InvFish = [4.0 * (1-x) for x in Flist]
	
	if PLOTONE:
		MasterNames = ["MyChi2"]
		MasterList = [MyChi2List]
		
	# MasterNames = ["MyChi2","ChiSq","ChiNorm ","Yates  ","YatesNorm","GList    "]
	# MasterList = [MyChi2List,ChiList,ChiListNorm,YatesList,YatesListNorm,GList]
	else:
		MasterNames = ["Concord","Gamma","MyChi2","ChiNorm ","YatesNorm","Residual"]
		MasterList = [ConList,GammaList,MyChi2List,ChiListNorm,YatesListNorm,RList]
		MasterNames = ["ChiList","YatesList","Concord","MyChi2","Fisher","Residual"]
		MasterList = [ChiList,YatesList,ConList,MyChi2List,InvFish,RList]
	
	NonZero = [MZ for MZ in MyChi2List if MZ > 0]

	if MyChi2List:
		GeometricMean = product(NonZero)**(1.0/len(NonZero))
		Average = sum(MyChi2List)/float(len(MyChi2List))
	else:
		GeometricMean = 0
		Average = 0

		
	# ScoreCount = sum([MC > Threshold for MC in MyChi2List])
	ThreshVar = MyChi2List
	OverThreshInd = [C for C,S in zip(range(len(ThreshVar)),ThreshVar) if S > Threshold]
	ScoreCount = len(OverThreshInd)
	OverThresh = []
	if ScoreCount:
		OverExpect = []
		OverActual = []
		for oi in OverThreshInd:
			OverThresh.append(NewCols[oi])
			OverActual.append(ActualList[oi])
			OverExpect.append(ECount[oi])
		if DEBUG:
			print "OT", len(set(OverThresh[0])), OverThresh
			print "OA", len(OverActual[0][0]), OverActual
			print "OE", OverExpect
	
	if DEBUG:
		for Tr,Co in zip(Traits,zip(*OverThresh)):
			print "  %s |\t"%Tr,"".join(Co)

		
	sys.stderr.write("** Total positions         :\t{0}\n".format(len(Cols)))
	sys.stderr.write("** Tested positions        :\t{0}\n".format(len(NewCols)))
	# sys.stderr.write("** Invariant column(s)     :\t{}\n".format(Invariant))
	# sys.stderr.write("** Column(s) with one diff :\t{}\n".format(SNPcount))
	# sys.stderr.write("** More than half dashes   :\t{}\n".format(DashyCount))
	sys.stderr.write("** Threshold value         :\t{:.2f} \n".format(Threshold))
	sys.stderr.write("** Positions over threshold:\t{0}\n".format(ScoreCount))
	sys.stderr.write("** Non-zero scores         :\t{} \n".format(len(NonZero)))
	sys.stderr.write("** Geometric mean score    :\t{:.2f} \n".format(GeometricMean))
	sys.stderr.write("** Average score           :\t{:.2f} \n".format(Average))
	
	
	# logout("ScoreCount",ScoreCount)
	for N,L in zip(MasterNames,MasterList):
		print "{}\t".format(N),
		printlists(L)
	
	if PLOTTHEM:
		from plotlists import plotlists
		FN = FileName or "test"
		plotlists(MasterNames,MasterList,FileName=FN,Cols=NewCols,Traits=Traits,Threshold=Threshold,yesPDF=True)
		
	if DEBUG or True:
		print "ExpectTables---------"
		# for y in Eli:
		# 	printlists(y)
		for z in ECount:
			printlists(z)
			print ". "*10
		
		print "ContingencyTables---------"
		for Act in ActualList:
			printlists(Act)
			print "..."

if __name__ == '__main__':
	main()


