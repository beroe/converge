library(ape)
library(geiger)
library(nlme)
library(phytools)
library(reshape2)

remove(guidetree)
remove(traittable)
remove(sites)

setwd("/Users/jwinnikoff/Documents/MBARI/converge/")

# Load simulated depth data
fakeTraits <- read.table("datasets/simDepths.tab", header=TRUE)

sites <- read.table("datasets/Converge_5col_example-JRW.tab", sep="\t", header=TRUE, row.names=1, stringsAsFactors = TRUE) 
sites <- Filter(function(x)(length(unique(x))>1), sites) # Remove 1-state columns
#column names prepended w/"X"

colnames(fakeTraits) <- sub("X","Y",colnames(fakeTraits))
sites$sp = row.names(sites)

guidetree <- read.tree("datasets/12.tre")
#guidetree$tip.label[9] <- "Deep0" # A name that's in Steve's dataset but not Joe's

# Drop tips from test guide tree that are not in the test alignment
for(missing in setdiff(guidetree$tip.label, sites$sp)){
  guidetree <- drop.tip(guidetree, missing)
}

traittable <- merge(fakeTraits,sites,by="sp")
row.names(traittable) <- traittable$sp

name.check(guidetree, traittable) # Make sure trait table and tree tips agree

## HYPOTHESIS TESTING ##
alpha <- 0.05 # Set significance cutoff

sites <- subset(sites, select = -c(sp)) # Drop sp column *after* merge
position <- as.numeric(substr(colnames(sites), 2, nchar(colnames(sites)))) # This is a numeric vector that excludes 1-state positions

glsList <- list("gls", length(colnames(sites)))
pValMin <- vector("numeric", length(colnames(sites)))
pValBonferroni <- vector("numeric", length(colnames(sites)))

for(i in 1:length(colnames(sites))){
  #model <- paste("log_depth ~ ",colnames(sites)[i],sep="")
  x <- as.matrix(traittable[ncol(fakeTraits)+1+i])
  names(x) <- row.names(traittable)
  y <- traittable$Y2
  names(y) <- row.names(traittable)
  glsModel <- phylANOVA(guidetree, x, y, nsim=1000, p.adj="bonferroni")
  #capture.output(summary(glsModel), file="/Users/jwinnikoff/Documents/MBARI/PGLS/unittest/PGLSmodels.txt", append=TRUE)
  pValBonferroni[i] <- min(glsModel$Pt)
  glsList[[i]] <- glsModel
}

alpha <- 0.05
neglog10pValBonferroni <- -log10(pValBonferroni)

position <- as.numeric(substr(colnames(sites), 2, nchar(colnames(sites))))

plotdata <- data.frame(position)
plotdata$neglog10pValBonferroni <- neglog10pValBonferroni
plotdata$pointColor <- ifelse(plotdata$neglog10pValBonferroni > -log10(.01), "cadetblue", ifelse(plotdata$neglog10pValBonferroni > -log10(alpha), "firebrick3", "black"))

pplot <- qplot(position,neglog10pValBonferroni, color = pointColor, data=plotdata)
pplot + scale_color_identity() + geom_hline(aes(yintercept=-log10(alpha), color="firebrick3")) + geom_hline(aes(yintercept=-log10(0.01), color="cadetblue")) + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),panel.background = element_blank(), axis.line = element_line(colour = "black"))