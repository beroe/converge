#! /usr/bin/env python
# -*- coding: utf-8 -*-

""" Make some fasta data with high/medium/low convergence """
import argparse

def get_options():
	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument("-m", "--multipy", type=int, dest="multiplier", default = 2, help="How many times to multiply the columns to make larger genes")	
	parser.add_argument("-b", "--debug", action="store_true", dest="DEBUG",default=False, help="Turn on Debugging output")
	parser.add_argument("-c", "--convergence", dest="converge", type=int,default=0,help="0,1,2 for Low medium, or high convergence")
	# parser.add_argument("-h", "--help", action="store_true", dest="HELP",default=False, help="Show help...")
	#parser.add_argument("FileList", nargs='+')
	options = parser.parse_args()
	return options

def MakeSimData(mult=2, Type=0):
	"""use multiplier times 10 as the total number of columns
	   Type = Low medium High 0,1,2"""
	# Columns 0-4 in order of best to worst scoring
	TypeColumns = ["AAEEEE","BBECCC","BBBCCC","ABCDEF","ACACAC"]
	TypeNames = ["Low","Med","High"]
	LMH   = [ [0,1,2,2,3,3,4,4,4,4],
	          [0,0,1,1,2,2,3,3,4,4],
	          [0,0,0,0,1,1,2,2,3,4] ]
	
	Traits = list("DDSSSS")
	Mat   = [TypeColumns[x] for x in LMH[Type]]   * mult
	
	Rows = zip(*Mat)
	OutStr = ""
	for N,TRow in enumerate(zip(Traits,Rows)):
		OutStr += ">{0}{1}-{2}".format(TRow[0],N,TypeNames[Type])
		OutStr += "\n"
		OutStr += "".join(TRow[1])
		OutStr += "\n"
				
	return OutStr

def main():
	Options = get_options()
	
	TypeList = [0,1,2]
	if Options.converge:
		TypeList = [Options.converge]
	for S in TypeList:
		Output = MakeSimData(mult=Options.multiplier,Type=S)
		print Output
	
#MC, MG = cs.MyChiSq2(Ol,El)  # MyChi and GStat
#RL     = cs.getResiduals(Ol,El)

if __name__ == '__main__':
	main()