#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

# 7-5 did not yield too many columns in my initial dataset
# '7-5' => [0,0,1,1,0,0,1,0,1,1,0,0],

our %CODES = ('6-6' => [1,0,1,1,0,0,1,0,1,1,0,0],
              '8-4' => [0,0,0,1,0,0,1,0,1,1,0,0],
              '9-3' => [0,0,0,0,0,0,1,0,1,1,0,0] );

MAIN: {
    my $file   = $ARGV[0] or usage();

    foreach my $split (keys %CODES) {
        $split =~ m/(\d)-(\d)/;
        my $split1 = $1;
        my $split2 = $2;
        my ($first_line,$ra_matrix,$ra_taxa) = get_matrix($file);
        my $ra_cols = get_bifurcating_cols($ra_matrix,$split,$split1,$split2);
print "$split\n";
print Dumper $ra_cols;
        print_matrix($first_line,"$file.$split1.$split2",$ra_matrix,$ra_taxa);
    }
}

sub print_matrix {
    my $first = shift;
    my $outfile = shift;
    my $ra_mat = shift;
    my $ra_taxa = shift;

    open OUT, ">$outfile" or die "cannot open >$outfile:$!";
    print OUT $first;
    for (my $i = 0; $i < @{$ra_taxa}; $i++) {
        print OUT sprintf("%-12s", $ra_taxa->[$i]);
        for (my $j = 0; $j < @{$ra_mat->[0]}; $j++) {
            print OUT $ra_mat->[$i]->[$j];
        }
        print OUT "\n";
    }
}

sub get_bifurcating_cols {
    my $ra_mat = shift;
    my $split = shift;
    my $s1 = shift;
    my $s2 = shift;
    my @cols = ();
    for (my $i = 0; $i < @{$ra_mat->[0]}; $i++) {
        my %dat = ();
        for (my $j = 0; $j < @{$ra_mat}; $j++) {
            $dat{$ra_mat->[$j]->[$i]}++;
        }
        next unless is_bifurcating(\%dat);
        if (bifurc_split(\%dat,$s1,$s2)) {
            push @cols, $i;
            my @keys = keys %dat;
            for (my $k = 0; $k < @{$CODES{$split}}; $k++) {
                $ra_mat->[$k]->[$i] = $keys[$CODES{$split}->[$k]];
            }
        }
    }
    return \@cols;
}

sub bifurc_split {
    my $rh_dat = shift;
    my $num1 = shift;
    my $num2 = shift;
    my @keys = keys %{$rh_dat};
#print "BLAH: $rh_dat->{$keys[0]}\n";
#print "BLAH: $rh_dat->{$keys[1]}\n";
    if (($rh_dat->{$keys[0]} == $num1 || $rh_dat->{$keys[1]} == $num1) &&
        ($rh_dat->{$keys[0]} == $num2 || $rh_dat->{$keys[1]} == $num2)) {
        return 1;
    } else {
        return 0;
    }
}

sub is_bifurcating {
    my $rh_dat = shift;
    return 1 if (scalar(keys(%{$rh_dat})) == 2);
    return 0;
}

sub get_matrix {
    my $file = shift;
    my @mat = ();
    my @taxa = ();
    open IN, $file or die "cannot open $file:$!";
    my $firstline = <IN>;
    while (my $line = <IN>) {
        chomp $line;
        my @fields = split /\s+/, $line; 
        my @seq = split /|/, $fields[1];
        push @mat, \@seq;
        push @taxa, $fields[0];
    }
    return ($firstline,\@mat,\@taxa);
}

sub usage {
    die "usage: $0 FILE\n";
}
